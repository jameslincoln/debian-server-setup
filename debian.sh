#!/bin/bash

# Add package sources
## PHP7.0
echo "deb http://packages.dotdeb.org jessie all" >> /etc/apt/sources.list
echo "deb-src http://packages.dotdeb.org jessie all" >> /etc/apt/sources.list
## MySQL5.7
echo "deb http://repo.mysql.com/apt/debian/ jessie mysql-apt-config" >> /etc/apt/sources.list
echo "deb http://repo.mysql.com/apt/debian/ jessie mysql-5.7" >> /etc/apt/sources.list
echo "deb http://repo.mysql.com/apt/debian/ jessie connector-python-2.0 connector-python-2.1 router-2.0 mysql-utilities-1.5 mysql-tools" >> /etc/apt/sources.list
echo "deb-src http://repo.mysql.com/apt/debian/ jessie mysql-5.7" >> /etc/apt/sources.list


# Add keys
apt-key add keys/dotdeb.gpg

# Update System and Install required tools
apt-get update && apt-get upgrade -y
apt-get install python-software-properties curl -y

# Apache2
apt-get install apache2 -y

# PHP7
apt-get install php7.0 php7.0-common php-pear php7.0-cli php7.0-mysql php7.0-json php7.0-mcrypt php7.0-mongodb php7.0-mbstring php7.0-zip php7.0-gd libapache2-mod-php7.0 -y

# MySQL5.7
apt-get install mysql-community-server -y

# Restart Apache2
service apache2 restart

# Setup SSH and secure it
ssh-keygen -t rsa -N $1 -f /root/.ssh/id_rsa
cp ssh-config/sshd_config /etc/ssh/sshd_config
service ssh restart

# Disable this.. (Answer NO)
dpkg-reconfigure dash

# Install and Configure fail2ban
apt-get install fail2ban -y
cp fail2ban-config/jail.conf /etc/fail2ban/jail.conf
service jail2ban restart

# Install Node.JS
curl -sL https://deb.nodesource.com/setup_7.x | bash -
apt-get install nodejs -y

# Install Yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
apt-get update && apt-get install yarn -y

# Composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '55d6ead61b29c7bdee5cccfb50076874187bd9f21f65d8991d46ec5cc90518f447387fb9f76ebae1fbbacf329e583e30') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
