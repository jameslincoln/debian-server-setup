#!/bin/bash

# Add package sources
## PHP7.0
add-apt-repository ppa:ondrej/php
## MySQL5.7
echo "deb http://repo.mysql.com/apt/ubuntu/ trusty mysql-apt-config" >> /etc/apt/sources.list
echo "deb http://repo.mysql.com/apt/ubuntu/ trusty mysql-5.7" >> /etc/apt/sources.list
echo "deb http://repo.mysql.com/apt/ubuntu/ trusty mysql-tools" >> /etc/apt/sources.list
echo "deb-src http://repo.mysql.com/apt/ubuntu/ trusty mysql-5.7" >> /etc/apt/sources.list


# Add keys
apt-key add keys/dotdeb.gpg

# Update System and Install required tools
apt-get update && apt-get upgrade -y
apt-get install python-software-properties curl -y

# Apache2
apt-get install apache2 -y

# PHP7
apt-get install php7.0 php7.0-common php-pear php7.0-cli php7.0-mysql php7.0-json php7.0-mcrypt php7.0-mongodb php7.0-mbstring php7.0-zip php7.0-gd libapache2-mod-php7.0 -y

# MySQL5.7
apt-get install mysql-community-server -y

# Restart Apache2
service apache2 restart

# Setup SSH and secure it
cp ssh-config/sshd_config /etc/ssh/sshd_config
service ssh restart

# Install and Configure fail2ban
apt-get install fail2ban -y
cp fail2ban-config/jail.conf /etc/fail2ban/jail.conf
service jail2ban restart

# Install Node.JS
curl -sL https://deb.nodesource.com/setup_7.x | bash -
apt-get install nodejs -y

# Install Yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
apt-get update && apt-get install yarn -y

# Composer
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=compose
