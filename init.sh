#!/bin/bash

# Add package sources
## PHP7.0
echo "deb http://packages.dotdeb.org jessie all" >> /etc/apt/sources.list
echo "deb-src http://packages.dotdeb.org jessie all" >> /etc/apt/sources.list
## MySQL5.7
echo "deb http://repo.mysql.com/apt/debian/ jessie mysql-apt-config" >> /etc/apt/sources.list
echo "deb http://repo.mysql.com/apt/debian/ jessie mysql-5.7" >> /etc/apt/sources.list
echo "deb http://repo.mysql.com/apt/debian/ jessie connector-python-2.0 connector-python-2.1 router-2.0 mysql-utilities-1.5 mysql-tools" >> /etc/apt/sources.list
echo "deb-src http://repo.mysql.com/apt/debian/ jessie mysql-5.7" >> /etc/apt/sources.list


# Add keys
apt-key add keys/dotdeb.gpg

# Update System and Install required tools
apt-get update && apt-get upgrade -y
apt-get install python-software-properties -y

# Apache2
apt-get install apache2 -y

# PHP7
apt-get install php7.0 php7.0-common php-pear php7.0-cli php7.0-mysql php7.0-json php7.0-mcrypt php7.0-mongodb libapache2-mod-php7.0

# MySQL5.7
apt-get install mysql-community-server

# Restart Apache2
service apache2 restart

# Setup SSH and secure it
ssh-keygen -t rsa -N $1 -f /root/.ssh/id_rsa
cp ssh-keygen/sshd_config /etc/ssh/sshd_config
service ssh restart

# Install and Configure fail2ban
apt-get install fail2ban -y
cp fail2ban/jail.conf /etc/fail2ban/jail.conf
